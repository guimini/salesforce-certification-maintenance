# Lightning Platform API Basics 

## Use REST API
### Learning Objectives
After completing this unit, you’ll be able to:
* Log in to Workbench and navigate to REST Explorer.

* Use the describe resource.

* Create an account using REST API.

* Execute a query using REST API.

```js
var nforce = require('nforce');
// create the connection with the Salesforce connected app
var org = nforce.createConnection({
  clientId: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET,
  redirectUri: process.env.CALLBACK_URL,
  mode: 'single'
});
// authenticate and return OAuth token
org.authenticate({
  username: process.env.USERNAME,
  password: process.env.PASSWORD+process.env.SECURITY_TOKEN
}, function(err, resp){
  if (!err) {
    console.log('Successfully logged in! Cached Token: ' + org.oauth.access_token);
    // execute the query
    org.query({ query: 'select id, name from account limit 5' }, function(err, resp){
      if(!err && resp.records) {
        // output the account names
        for (i=0; i<resp.records.length;i++) {
          console.log(resp.records[i].get('name'));
        }
      }
    });
  }
  if (err) console.log(err);
});
```

## Use SOAP API

### Learning Objectives
After completing this unit, you’ll be able to:

* Generate a WSDL file for your org.

* Use SoapUI to create a SOAP project from the WSDL file.

* Log in to your Trailhead Playground using SOAP API.

* Create an account using SOAP API.

### Steps

* Generate WSDL file  
* Reset password token  
* Login with saop tools.

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:enterprise.soap.sforce.com">
   <soapenv:Body>
      <urn:login>
         <urn:username>mcicheick@resourceful-moose-jkv1cm.com</urn:username>
         <urn:password>mypasswordh3oiLe7i2E7Yzk18TXmDT7KU</urn:password>
      </urn:login>
   </soapenv:Body>
</soapenv:Envelope>
```

```xml
<instance>resourceful-moose-jkv1cm-dev-ed.my</serverUrl>
<sessionId>00D1t000000rixQ!AQUAQJx7hw1LAkwt.m_8U0nY82RGP2sjsTeiE5.NYOGqBZSI_2ouIQr1liFX1H1F990E8aOAAM7GtftZZkBBFpG0_S7qbUO3</sessionId>
```

* SAOP Request `https://resourceful-moose-jkv1cm-dev-ed.my.salesforce.com/services/Soap/c/46.0`

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:enterprise.soap.sforce.com" xmlns:urn1="urn:sobject.enterprise.soap.sforce.com">
   <soapenv:Header>
      <urn:SessionHeader>
         <urn:sessionId>00D1t000000rixQ!AQUAQJx7hw1LAkwt.m_8U0nY82RGP2sjsTeiE5.NYOGqBZSI_2ouIQr1liFX1H1F990E8aOAAM7GtftZZkBBFpG0_S7qbUO3</urn:sessionId>
      </urn:SessionHeader>
   </soapenv:Header>
   <soapenv:Body>
      <urn:create>
         <!--Zero or more repetitions:-->
         <urn:sObjects xsi:type="urn1:Account" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <!--Zero or more repetitions:-->
            <Name>Sample SOAP Account</Name>
         </urn:sObjects>
      </urn:create>
   </soapenv:Body>
</soapenv:Envelope>
```

## Use Bulk API

### Learning Objectives
After completing this unit, you’ll be able to:

* Describe how an asynchronous request differs from a synchronous request.

* Create a bulk job using REST Explorer in Workbench.

* Import data to your Salesforce org by adding data to a job.

* Monitor a job’s progress.

* Get a job’s results.

### Steps

* Create a job `/services/data/v45.0/jobs/ingest`  
```json
{
  "operation" : "insert",
  "object" : "Account",
  "contentType" : "CSV",
  "lineEnding" : "CRLF"
}
```

* PUT `/services/data/v45.0/jobs/ingest/7501t000006iiU8AAI/batches` to add data.  
```csv
"Name"
"Sample Bulk API Account 1"
"Sample Bulk API Account 2"
"Sample Bulk API Account 3"
"Sample Bulk API Account 4"
```

* PATCH `/services/data/v45.0/jobs/ingest/7501t000006iiU8AAI` to complete job.  
```json
{
   "state" : "UploadComplete"
}
```

* GET `/services/data/v45.0/jobs/ingest/7501t000006iiU8AAI/successfulResults` after data loaded get status.

## Use Streaming API

### Learning Objectives
After completing this unit, you’ll be able to:
* Describe the primary benefit that push technology offers over pull technology.

* Create a PushTopic and receive event notifications.

* Define a platform event and derive the subscription channel.

* Broadcast a message with generic streaming.

* Specify replay options for durable streaming.

### Quiz

1
Streaming API's push paradigm lets you:

A
Create more records in a single API call

B
Use SOSL to listen for event notifications

C
Avoid making unnecessary API requests by listening for notifications rather than polling for data

D
Write code from the crow's nest of a pirate ship

2
Why isn't the following SOQL query a valid PushTopic query? SELECT Name, Phone FROM Contact WHERE MailingCity='Indianapolis'

A
WHERE clauses aren't supported for PushTopics.

B
The SELECT statement doesn't include an ID.

C
Contact isn't a supported object for PushTopic queries.

D
The query is valid.

3
What channel name corresponds to a platform event that you defined with the label of Solar Panel Event?

A
/event/Solar_Panel_Event

B
/topic/Solar_Panel_Event

C
/event/Solar_Panel_Event__c

D
/event/Solar Panel Event

E
/event/Solar_Panel_Event__e

4
How do you broadcast a message with generic streaming?

A
Create a generic streaming channel, and then POST a request to /StreamingChannel/<streaming channel ID>/push.

B
Create a PushTopic, and then POST a request to /PushTopic/<PushTopicId>/push.

C
POST a request to /PushTopic/push.

D
POST a request to /StreamingChannel/push.

5
Which replay option specifies that the subscriber receives event notifications with replay IDs 6, 7, 8, and 9?

A
6

B
5

C
-2

D
-1

### Answers

1. C  
2. B  
3. E  
4. A  
5. B

# Lightning Flow 

## Guide Users Through Your Business Processes with Flow Builder

### Learning Objectives
After completing this unit, you'll be able to:

Define a flow and list its key components.
Describe the types of flow elements.
Build a flow that creates a record and uploads files.

### Steps

* Create new Flow  
* Add Flow components  
* Create Home page  (Lightning)  
* Add New Flow.

## Combine the Power of Process Builder and Flow Builder

### Learning Objectives
After completing this unit, you'll be able to:

Describe a business process that can be automated using a process and a flow.
Define what a flow variable is.
Build a flow that iterates over a group of records.
Build a process that starts a flow.

### Quiz

1
A record variable can store:

A
A set of field values for a single record.

B
A set of field values for multiple records that have the same object type.

C
A set of field values for multiple records that have multiple object types.

D
A and B

2
For which use case is it appropriate to combine a process and a flow?

A
Post to an internal Chatter group.

B
Clone a record and its children.

C
Delete a related record.

D
B and C

3
Inside a loop, you should avoid:

A
Executing actions, like creating or updating records.

B
Assigning new values to variables.

C
Displaying data to the user.

D
Nesting another loop.

### Answers

1. A  
2. D  
3. A

## Customize How Records Get Approved with Approvals
### Learning Objectives
After completing this unit, you'll be able to:
Define an approval process, and list its key components.
Describe a business process that can be automated using an approval process.
Set up an approval process to automatically manage when an account changes from a prospect to a new customer.

### Steps


# Data Integration Specialist

* Insrtall Unmanaged package.  
* Create custom settings, remote site settings, named credentials

* https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_rest_code_sample_restrequest.htm
* https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/langCon_apex_transaction_control.htm

