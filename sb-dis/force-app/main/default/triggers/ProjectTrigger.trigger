trigger ProjectTrigger on Project__c (after update) {
    List<String> projectIds = new List<String>();
    
    for(Integer i = 0; i < Trigger.new.size(); i++) {
        if(Trigger.new[i].Status__c == 'Billable' && Trigger.new[i].Status__c != Trigger.old[i].Status__c) {
            projectIds.add(Trigger.new[i].Id);
        }
    }
    if(!projectIds.isEmpty()) BillingCalloutService.callBillingService(projectIds);
}