@RestResource(urlMapping='/project')
global with sharing class  ProjectRESTService {
    //Implement service logic here
    @HttpPost
    global static String postProjectData(String ProjectRef, String ProjectName, String OpportunityId, Date StartDate, Date EndDate, Double Amount, String Status) {
        // SELECT Id, Name, Opportunity__c, Billable_Amount__c, End_Date__c, ProjectRef__c, Start_Date__c, Status__c FROM Project__c
        Project__c project = new Project__c(ProjectRef__c = ProjectRef,
                                            Name = ProjectName,
                                            Opportunity__c = OpportunityId,
                                            Start_Date__c = StartDate,
                                            End_Date__c = EndDate,
                                            Billable_Amount__c = Amount,
                                            Status__c = Status);
        Savepoint sp = Database.setSavepoint();
        try {
            upsert project ProjectRef__c;
            Opportunity opp = new Opportunity(Id = OpportunityId, DeliveryInstallationStatus__c = 'In progress');
            update opp;
            return 'OK';
        } catch(Exception e) {
            Database.rollback(sp);
            return e.getMessage();
        }
    }
}