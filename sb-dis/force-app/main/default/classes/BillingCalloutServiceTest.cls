@isTest
private class BillingCalloutServiceTest {
    private static Project__c generateProject() {
        ServiceCredentials__c service = new ServiceCredentials__c(Name = 'BillingServiceCredential', Username__c = 'Test', Password__c = 'Test');
        insert service;
        Account a = new Account(Name = 'ACME Corp, Inc.');
        insert a;
        Opportunity opp = new Opportunity(Name = 'Super-Duper Big Deal',
                                          AccountId = a.Id,
                                          CloseDate = System.today(),
                                          Amount=150000,
                                          Type = 'New Project',
                                          StageName = 'Prospecting');
        insert opp;
        String	ProjectRef = '12345678';
        String	ProjectName = 'Project 1';
        String	OpportunityId = opp.Id;
        Date	StartDate = System.today();
        Date	EndDate = System.today();
        Double	Amount = 150000;
        Project__c proj = new Project__c(ProjectRef__c = ProjectRef,
                                            Name = ProjectName,
                                            Opportunity__c = OpportunityId,
                                            Start_Date__c = StartDate,
                                            End_Date__c = EndDate,
                                            Billable_Amount__c = Amount,
                                            Status__c = 'Running');
        insert proj;
        return proj;
    }
    
    @IsTest
    static void normalTest() {
        Project__c proj = generateProject();
        Test.startTest();
        update proj;
        Test.stopTest();
        Project__c retrieved = [Select Id, Status__c From Project__c Where Id = :proj.Id];
        System.assertEquals('Running', retrieved.Status__c);
    }
    
    @IsTest
    static void successCalloutTest() {
        Test.setMock(WebServiceMock.class, new BillingCalloutServiceMock());
        Project__c proj = generateProject();
        Test.startTest();
        proj.Status__c = 'Billable';
        update proj;
        Test.stopTest();
        Project__c retrieved = [Select Id, Status__c From Project__c Where Id = :proj.Id];
        System.assertEquals('Billed', retrieved.Status__c);
    }
    
    @IsTest
    static void failedCalloutTest() {
        Test.setMock(WebServiceMock.class, new BillingCalloutServiceMockFailure());
        Project__c proj = generateProject();
        Test.startTest();
        proj.Status__c = 'Billable';
        update proj;
        Test.stopTest();
        Project__c retrieved = [Select Id, Status__c From Project__c Where Id = :proj.Id];
        System.assertEquals('Billable', retrieved.Status__c);
    }
}