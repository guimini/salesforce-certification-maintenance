public class ProjectCalloutService {
    //Complete the implementation for business and callout logic
	@InvocableMethod
    public static void PostOpportunityToPMS(List<Id> opportunityId) {
        List<Opportunity> opps = [Select Id, Name, Account.Name, CloseDate, Amount From Opportunity Where Id IN :opportunityId];
        if(!opps.isEmpty()) System.enqueueJob(new QueueablePMSCall(opps[0]));
    }
    
    public class QueueablePMSCall implements Queueable, Database.AllowsCallouts {
        
        private Opportunity opp;
        public QueueablePMSCall(Opportunity opp) {
            this.opp = opp;
        }
        
        /**
         * {
            "opportunityId": "00641000004EKmFAAW",
            "opportunityName": "Super-Duper Big Deal",
            "accountName": "ACME Corp, Inc.",
            "closeDate": "2016-10-30",
            "amount": 150000
           }
         */
        public void execute(QueueableContext context){
            HttpRequest req = new HttpRequest();
            req.setEndpoint('callout:ProjectService');
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            Map<String, ServiceTokens__c> setting = ServiceTokens__c.getAll();
            ServiceTokens__c tokenSetting = setting.get('ProjectServiceToken');                              
            String token = tokenSetting.Token__c;
            req.setHeader('token', token);
            Map<String, Object> data = new Map<String, Object>();
            data.put('opportunityId', opp.Id);
            data.put('opportunityName', opp.Name);
            data.put('accountName', opp.Account.Name);
            data.put('closeDate', opp.CloseDate);
            data.put('amount', opp.Amount);
            String body = JSON.serialize(data);
            req.setBody(body);
            Http http = new Http();
            HTTPResponse res = http.send(req);
            opp.StageName = res.getStatusCode() == 201 ? 'Submitted Project' : 'Resubmit Project';
            update opp;
        }
    }
}