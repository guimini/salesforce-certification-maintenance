global class BillingCalloutService {
    @future(callout = true)
    global static void callBillingService(List<String> projectIds) {
        List<Project__c> projects = [Select Id, ProjectRef__c, Billable_Amount__c From Project__c Where Id In :projectIds Limit 1];
        Map<String, ServiceCredentials__c> settings = ServiceCredentials__c.getAll();
        ServiceCredentials__c service = settings.get('BillingServiceCredential');
        BillingServiceProxy.project project = new BillingServiceProxy.project();
        project.username = service.Username__c;
        project.password = service.Password__c;
        project.projectid = projects[0].ProjectRef__c;
        project.billAmount = projects[0].Billable_Amount__c;
        BillingServiceProxy.InvoicesPortSoap11 invoiceService = new BillingServiceProxy.InvoicesPortSoap11();
        String result = invoiceService.billProject(project);
        System.debug('result = ' + result);
        if(result == 'ok') {
            Project__c toUpdate = new Project__c(Id = projects[0].Id, Status__c = 'Billed');
            update toUpdate;
        }
    }
}