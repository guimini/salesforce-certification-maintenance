@isTest
private class ProjectRESTServiceTest {
    private static Opportunity createData(String StageName) {
        Account a = new Account(Name = 'ACME Corp, Inc.');
        insert a;
        Opportunity opp = new Opportunity(Name = 'Super-Duper Big Deal',
                                          AccountId = a.Id,
                                          CloseDate = System.today(),
                                          Amount=150000,
                                          Type = 'New Project',
                                          StageName = StageName);
        return opp;
    }
    
    @IsTest
    static void successPostProjectData() {
        Opportunity opp = createData('Prospecting');
        insert opp;
        String	ProjectRef = '12345678';
        String	ProjectName = 'Project 1';
        String	OpportunityId = opp.Id;
        Date	StartDate = System.today();
        Date	EndDate = System.today();
        Double	Amount = 150000;
        String	Status = 'Billable';
        ProjectRESTService.postProjectData(ProjectRef, ProjectName, OpportunityId, StartDate, EndDate, Amount, Status);
        Project__c project = [SELECT Id, Name, Opportunity__c, Billable_Amount__c, End_Date__c, ProjectRef__c, Start_Date__c, Status__c FROM Project__c Where ProjectRef__c = :ProjectRef];
    	System.assertEquals(ProjectName, project.Name);
        Opportunity op = [Select Id, DeliveryInstallationStatus__c From Opportunity Where Id = :opp.Id];
        System.assertEquals('In progress', op.DeliveryInstallationStatus__c);
    }
    
    @IsTest
    static void existingProjectPostProjectData() {
        Opportunity opp = createData('Prospecting');
        insert opp;
        String	ProjectRef = '12345678';
        String	ProjectName = 'Project 1';
        String	OpportunityId = opp.Id;
        Date	StartDate = System.today();
        Date	EndDate = System.today();
        Double	Amount = 150000;
        String	Status = 'Billable';
        Project__c proj = new Project__c(ProjectRef__c = ProjectRef,
                                            Name = ProjectName,
                                            Opportunity__c = OpportunityId,
                                            Start_Date__c = StartDate,
                                            End_Date__c = EndDate,
                                            Billable_Amount__c = Amount,
                                            Status__c = Status);
        insert proj;
        Status = 'Billed';
        String response = ProjectRESTService.postProjectData(ProjectRef, ProjectName, OpportunityId, StartDate, EndDate, Amount, Status);
        System.assertEquals('OK', response);
        System.assertEquals(1, [Select count() From Project__c], 'No new creation.');
        Project__c project = [SELECT Id, Status__c FROM Project__c Where ProjectRef__c = :ProjectRef];
    	System.assertEquals(Status, project.Status__c);
    }
    
    @IsTest
    static void failureProjectPostProjectData() {
        Opportunity opp = createData('Prospecting');
        insert opp;
        String	ProjectRef = '';
        String	ProjectName = 'Project 1';
        String	OpportunityId = opp.Id;
        Date	StartDate = System.today();
        Date	EndDate = System.today();
        Double	Amount = 150000;
        String	Status = 'Billable';
        ProjectRESTService.postProjectData(ProjectRef, ProjectName, OpportunityId, StartDate, EndDate, Amount, Status);
        System.assertEquals(0, [Select count() From Project__c], 'No new creation.');
        Opportunity op = [Select Id, DeliveryInstallationStatus__c From Opportunity Where Id = :opp.Id];
        System.assertEquals(null, op.DeliveryInstallationStatus__c);
    }
}