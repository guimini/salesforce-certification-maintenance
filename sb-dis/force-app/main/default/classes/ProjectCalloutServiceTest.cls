@isTest
private class ProjectCalloutServiceTest {
    private static Opportunity createData(String StageName) {
        Account a = new Account(Name = 'ACME Corp, Inc.');
        insert a;
        /*
        {
          "opportunityId": "00641000004EKmFAAW",
          "opportunityName": "Super-Duper Big Deal",
          "accountName": "ACME Corp, Inc.",
          "closeDate": "2016-10-30",
          "amount": 150000
        }*/
        Opportunity opp = new Opportunity(Name = 'Super-Duper Big Deal',
                                          AccountId = a.Id,
                                          CloseDate = System.today(),
                                          Amount=150000,
                                         Type = 'New Project',
                                         StageName = 'Prospecting');
        insert opp;
        ServiceTokens__c token = new ServiceTokens__c();
        token.Name = 'ProjectServiceToken';
        token.Token__c = 'Token_value';
        insert token;
        Test.startTest();
        opp.StageName = StageName;
        update opp;
        Test.stopTest();
        return opp;
    }
    
    @IsTest
    static void postOpportunityToPMS() {
        Opportunity opp = createData('Qualification');
        Opportunity createdOpp = [Select Id, StageName From Opportunity Where Id = :opp.Id];
        System.assertEquals('Qualification', createdOpp.StageName);
    }
    
    @IsTest
    static void successPostOpportunityToPMS() {
        Test.setMock(HttpCalloutMock.class, new ProjectCalloutServiceMock());
        Opportunity opp = createData('Closed Won');
        Opportunity createdOpp = [Select Id, StageName From Opportunity Where Id = :opp.Id];
        System.assertEquals('Submitted Project', createdOpp.StageName);
    }
    
    @IsTest
    static void failurePostOpportunityToPMS() {
        Test.setMock(HttpCalloutMock.class, new ProjectCalloutServiceMockFailure());
        Opportunity opp = createData('Closed Won');
        Opportunity createdOpp = [Select Id, StageName From Opportunity Where Id = :opp.Id];
        System.assertEquals('Resubmit Project', createdOpp.StageName);
    }
}