public class ProjectCalloutServiceMockFailure implements HttpCalloutMock {
   public HTTPResponse respond(HTTPRequest req) {        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('Error !');
        res.setStatusCode(500);
        return res;
    }
}