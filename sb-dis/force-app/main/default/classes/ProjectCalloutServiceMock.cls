@IsTest
public class ProjectCalloutServiceMock implements HttpCalloutMock {
   	public HTTPResponse respond(HTTPRequest req) {        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('OK');
        res.setStatusCode(201);
        return res;
    }
}