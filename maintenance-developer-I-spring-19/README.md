# Platform Developer I Certification Maintenance (Spring ‘19)

## Learn What’s New for Platform Developers in Spring ’19

### Learning Objectives
After completing this unit, you’ll be able to:

* Learn about the new Apex methods, exceptions, and interfaces.  
* Understand a new way that unhandled Apex exception details are exposed.  
* Use the new sandbox cloning feature.  
* Configure Default Field Values with Custom Metadata Types.  

### Questions
#### 1. Which Apex interface can be implemented to allow My Domain users to log in with something other than their username and password?
A. Auth.AuthToken  
B. Auth.VerificationMethod  
C. Auth.LoginDiscoveryHandler  
D. Auth.MyDomainLoginDiscoveryHandler  

#### 2. With Spring '19, which method returns a list of OrgLimit instances used to investigate limits and their names, current value, and maximum value?
A. getAll() from the System.OrgLimit Class  
B. getAll() from the System.OrgLimits Class  
C. getInstances() from the System.OrgLimit Class  
D. getInstances() from the System.OrgLimits Class  

#### 3. With Spring '19, which properties of an unhandled Apex exception are available in Event Monitoring log files?
A. Static variable state and stack trace  
B. Exception type, name, and static variable state  
C. Stack trace, user's location, and exception type  
D. Exception message, exception type name, and stack trace   

#### 4. Which field of the SandboxInfo object is a reference to the ID of the SandboxInfo that served as the source org for a cloned sandbox?
A. SourceId  
B. TemplateId  
C. SandboxName  
D. SandboxInfoId  

#### 5. You created a custom metadata type to handle your company's warranty policy. The custom metadata type's label is WarrantyRule. For it, you created a custom field labeled Warranty and a metadata record labeled Gold. What is the correct syntax to reference the value stored in the Gold metadata record?
A. $WarrantyRule.Gold.Warranty__c  
B. $WarrantyRule__mdt.Gold.Warranty  
C. $CustomMetadata.WarrantyRule.Gold.Warranty  
D. $CustomMetadata.WarrantyRule__mdt.Gold.Warrantyc  

### Answers
1. D  
2. B  
3. D  
4. A  
5. D  

## Work with the New Apex Security Settings
### Learning Objectives

After completing this unit, you’ll be able to:

* Understand the intended use of SOQL WITH SECURITY_ENFORCED clause.
* Work with the New Apex Security Settings.

### Exercise

* ApexClass : [SecureApexRest](force-app/main/default/classes/SecureApexRest.cls)  
* CustomField : [Secret_Key](force-app/main/default/objects/Contact/fields/Secret_Key__c.field-meta.xml)

